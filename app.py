import time, datetime
import requests
import json
import argparse

long_polling_url = 'http://stream.meetup.com/2/rsvps'


def main(collection_time=60, top_count=3):
    furthest_event_time = 0
    furthest_event_url = ''
    rsvp_count = 0
    event_host_dict = {}

    response = requests.get(long_polling_url, stream=True)

    start_time = time.time()

    for rsvp in response.iter_lines():

        # Exit loop after collection time has lapsed
        if time.time() - start_time > collection_time:
            response.close()
            break

        # Decode to UTF 8
        rsvp = json.loads(rsvp.decode('utf8'))

        # Increment the rsvp counter
        rsvp_count += 1

        # Set the future event time and url
        event_dict = rsvp.get('event', {})
        if event_dict:
            event_time = event_dict.get('time', 0)
            if event_time > furthest_event_time:
                furthest_event_time = event_time
                furthest_event_url = event_dict.get('event_url', '')

        # Track host and number of occurrences
        group_dict = rsvp.get('group', {})
        if group_dict:
            host_country = group_dict.get('group_country')
            event_host_dict[host_country] = event_host_dict.get(host_country, 0) + 1

    # Determine the top rsvps by country
    top_rsvps_by_country = get_top_rsvp_by_country(event_host_dict, top_count)

    # Generate output
    # convert time in milliseconds to time stamp
    furthest_event_time = datetime.datetime.fromtimestamp(furthest_event_time / 1000.0).strftime('%Y-%m-%d %H:%M')
    result = '{0},{1},{2},{3}'.format(rsvp_count, furthest_event_time, furthest_event_url, ','.join("{0},{1}".format(h, c) for h, c in top_rsvps_by_country))

    return result


def get_top_rsvp_by_country(event_host_dict, top_count):
    result = []
    # In the case we get less than top_count, limit to the size of the dictionary
    top_count = top_count if len(event_host_dict.keys()) > top_count else len(event_host_dict.keys())
    for i in range(top_count):
        host_key, host_count = max(event_host_dict.items(), key=lambda t: t[1])
        result.append((host_key, host_count))
        # set the count of the current max to 0 so it can be ignored
        event_host_dict[host_key] = 0

    return result


if __name__ == '__main__':

    collection_time = 60
    top_count = 3

    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--time", help="Time to collect RSVPs")
    parser.add_argument("-c", "--count", help="Max number of top host countries to return")
    args = parser.parse_args()

    try:

        if args.time:
            collection_time = int(args.time)

        if args.count:
            top_count = int(args.count)

        aggregate_output = main(collection_time, top_count)
        print(aggregate_output)

    except Exception as ex:
        print(str(ex))
