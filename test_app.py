import unittest
from unittest import mock
from app import main
import json
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
test_file_path = os.path.join(dir_path, 'test.json')


# Return a mock response object
def get_mock_request(*args, **kwargs):
    class MockResponse:

        def __init__(self, status_code):
            self.status_code = status_code
            self.rsvps = []
            # Load the test rsvps
            fp = open(test_file_path, 'r')
            for line in fp.readlines():
                line = json.loads(line)
                line = str.encode(line)
                self.rsvps.append(line)
            fp.close()

        # Return the list of rsvps from the file for the iter_lines
        def iter_lines(self):
            return self.rsvps

    return MockResponse(200)


class TestSum(unittest.TestCase):

    @mock.patch('requests.get', side_effect=get_mock_request)
    def test_main(self, get_mock):
        agg_output = main()
        agg_out_list = agg_output.split(',')
        self.assertEqual(agg_out_list[0], '11', "No. of RSVPs should be 11")
        self.assertEqual(agg_out_list[1], '2019-08-03 09:00', "Furthest time should be 2019-08-03 09:00")
        self.assertEqual(agg_out_list[2], 'https://www.meetup.com/Its-better-Outdoors/events/261259818/',
                         "Event URL should be https://www.meetup.com/Its-better-Outdoors/events/261259818")
        self.assertEqual(agg_out_list[3], 'us', "Top host country should be us")
        self.assertEqual(agg_out_list[4], '5', "Top host country RSVPs should be 5")


if __name__ == '__main__':
    unittest.main()
